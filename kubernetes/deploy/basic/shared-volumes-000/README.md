# Run

```
kubectl create -f multi-container-shared-volume.yaml
```

# Get endpoint

```
kubectl \
  -n multi-container-shared-volume \
  describe service my-nginx \
  |grep "LoadBalancer Ingress"
```

# View shared file

```
kubectl \
  -n multi-container-shared-volume \
  exec my-nginx-[PRESS TAB TO AUTOCOMPLETE] \
  -c 1st -- /bin/cat /usr/share/nginx/html/index.html
```

# Cleanup

```
kubectl delete namespace multi-container-shared-volume
```

# Reference

http://devops.buzz/kubernetes-deploy-multi-container-aplication-with-shared-volumes/
