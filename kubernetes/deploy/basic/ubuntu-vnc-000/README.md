# Run

```
kubectl create -f ubuntu-vnc.yaml
```

# Get endpoint

```
kubectl \
  -n ubuntu-vnc \
  describe service my-nginx \
  |grep "LoadBalancer Ingress"
```

# Cleanup

```
kubectl delete -f ubuntu-vnc.yaml
```

# Reference

https://hub.docker.com/r/chenjr0719/ubuntu-unity-novnc/
