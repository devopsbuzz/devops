# Run

```
kubectl create -f basic-nginx.yaml
```

# Get endpoint

```
kubectl \
  -n basic-nginx \
  describe service my-nginx \
  |grep "LoadBalancer Ingress"
```

# View shared file

```
kubectl \
  -n basic-nginx \
  exec my-nginx-[PRESS TAB TO AUTOCOMPLETE] \
  -c 1st -- /bin/cat /usr/share/nginx/html/index.html
```

# Cleanup

```
kubectl delete -f basic-nginx.yaml
```
