# Run

```
kubectl create -f multi-container-ipc-nginx-proxy.yaml
```

# Get endpoint

```
kubectl \
  -n multi-container-ipc-nginx-proxy \
  describe service my-nginx \
  |grep "LoadBalancer Ingress"
```

# Cleanup

```
kubectl delete namespace multi-container-ipc-nginx-proxy
```

# Reference

http://devops.buzz/kubernetes-deploy-multi-container-aplication-with-inter-process-communications-ipc/
