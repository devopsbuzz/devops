# Run

```
kubectl create -f docker-inside-docker.yaml
```

# Get endpoint

```
kubectl \
  -n docker-inside-docker \
  describe service my-nginx \
  |grep "LoadBalancer Ingress"
```

# View shared file

```
kubectl \
  -n docker-inside-docker \
  exec my-nginx-[PRESS TAB TO AUTOCOMPLETE] \
  -c 1st -- /bin/cat /usr/share/nginx/html/index.html
```

# Cleanup

```
kubectl delete -f docker-inside-docker.yaml
```

# Reference

http://devops.buzz/kubernetes-deploy-multi-container-aplication-with-shared-volumes/
