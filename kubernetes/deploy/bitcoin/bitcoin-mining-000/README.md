# Run

```
kubectl create -f kubernetes-bitcoin-mining.yaml
```

# Scale

```
kubectl -n kubernetes-bitcoin-mining scale deployment my-btc --replicas=100
```

# Cleanup

```
kubectl delete namespace kubernetes-bitcoin-mining
```

# Reference

https://github.com/alexellis/mine-with-docker

https://www.nicehash.com/asic-mining
